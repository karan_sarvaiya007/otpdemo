//
//  ViewController.swift
//  OtpDemo
//
//  Created by Yogesh Jethva on 12/11/19.
//  Copyright © 2019 Yogesh Jethva. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtFld1: UITextField!
    @IBOutlet weak var txtFld2: UITextField!
    @IBOutlet weak var txtFld3: UITextField!
    @IBOutlet weak var txtFld4: UITextField!
    @IBOutlet weak var lblMinTimer: UILabel!
    @IBOutlet weak var lblVerificationSent: UILabel!
    
    var minCounter = 10
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let mainattri = NSMutableAttributedString(string: "A text message with a verification code has been sent to ", attributes: [NSAttributedString.Key.foregroundColor:UIColor.lightGray,NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        
        let numAttri = NSAttributedString(string: "+911234567890", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 15)])
        
        mainattri.append(numAttri)
        
        lblVerificationSent.attributedText = mainattri
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(minTimerChange), userInfo: nil, repeats: true)
        
        
        txtFld1.delegate = self
        txtFld2.delegate = self
        txtFld3.delegate = self
        txtFld4.delegate = self
        
        txtFld1.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtFld2.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtFld3.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtFld4.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
    }
    @objc func minTimerChange() {
        if minCounter > 0 {
            minCounter -= 1
            lblMinTimer.text = "00:\(minCounter)"
        }else{
            print("final min : \(minCounter)")
            timer?.invalidate()
            timer = nil
        }
    }
    
    
}

extension ViewController:UITextFieldDelegate {
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        if text?.count ?? 0 >= 1{
            switch textField{
            case txtFld1:
                txtFld2.becomeFirstResponder()
            case txtFld2:
                txtFld3.becomeFirstResponder()
            case txtFld3:
                txtFld4.becomeFirstResponder()
            case txtFld4:
                txtFld4.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
}
